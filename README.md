# FLutter Projet

## Version flutter
Version de flutter : 3.0.0

## Dependencies
- cupertino_icons: ^1.0.2
- http: ^0.13.4
- bloc: ^8.0.3
- flutter_bloc: ^8.0.1
- latlong2: ^0.8.1
- location: ^4.4.0
- geolocator: ^8.2.1
- collection: ^1.16.0
- google_maps_flutter: ^2.1.7
- dio: ^4.0.6
- flutter_polyline_points: ^1.0.0
- shared_preferences: ^2.0.15

## Fonctionnalités
L'application est composée de 3 écrans. L'écran Home et Favoris, et une Stack sur Home.
L'application peut fonctionner sans la localisation, mais il est conseillé de l'activer pour accéder à plus d'informations.
Au lancement de l'application on se retrouve sur l'écran Home.

#### Home : 
- On récupère la liste des installations sportives de la ville d'Angers depuis l'API, et on les affiches sous formes de Card.
- En utilisant la localisation du téléphone, les installations sont triées par distance.
- Si l'installation sportive contient plusieurs activités alors au lieu d'afficher une seule activité dans la Card, au onTap on affiche avec un ValueListenableBuilder (pour réactualiser sans tout rebuild) la liste des activités.
- Au onTap sur une activité on navigue vers le Detail de l'activité.

#### Detail
- Si la localisation est activée, on se connecte à l'API Google Maps, et on affiche une carte.
    - La carte contient 2 markers, 1 qui indique ma position, et le deuxième qui indique l'installation sportive
    - En utlisant l'API Directions, on récupère les polylines, et on affiche sur la carte l'itinéraire vers l'installation sportive, on affiche également au dessus de la carte la distance de trajet et le temps de trajet.
- On affiche en dessous de carte des éléments descriptifs de l'activité
- Dans l'appBar on une bouton options qui permet d'ajouter/supprimer l'activité des Favoris (stocker en local avec SharedPreferences).

#### Favoris
- De la même manière que la page Home, on affiche une liste de Card correspondants au installations sportives qui contiennent au moins une catégorie en tant que favoris.

## API utilisées
- site : https://data.angers.fr/explore/dataset/equipements-sportifs-angers/information/?location=17,47.47032,-0.52178&basemap=jawg.streets
- requête : https://data.angers.fr/api/records/1.0/search/?dataset=equipements-sportifs-angers&q=&rows=2500&facet=categorie&facet=nom_fam_eq&facet=equip_2&facet=angers_stadium
#### Pour la carte Google Maps (géré depuis Google Cloud Platform) 
- Maps SDK for iOS
- Maps SDK for Android
- Directions API		
