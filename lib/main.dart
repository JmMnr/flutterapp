import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_projet_final/bloc/favorites_list_cubit.dart';
import 'package:flutter_projet_final/bloc/installation_sportive_list_cubit.dart';
import 'package:flutter_projet_final/bloc/location_cubit.dart';
import 'package:flutter_projet_final/repository/installation_sportive_repository.dart';
import 'package:flutter_projet_final/ui/screens/bottom_navigation.dart';
import 'package:flutter_projet_final/ui/screens/installation_sportive_list.dart';

import 'ui/screens/installation_sportive_detail.dart';

void main() {
  InstallationSportiveRepository installationSportiveRepository = InstallationSportiveRepository();
  InstallationSportiveCubit installationSportiveCubit = InstallationSportiveCubit(installationSportiveRepository);
  LocationCubit locationCubit = LocationCubit();
  FavoritesListCubit favoritesListCubit = FavoritesListCubit();
  runApp(MultiBlocProvider(providers: [
    BlocProvider<InstallationSportiveCubit>(
    create: (_) => installationSportiveCubit
    ),
    BlocProvider<LocationCubit>(
    create: (_) => locationCubit
    ),
    BlocProvider<FavoritesListCubit>(
    create: (_) => favoritesListCubit
    )
    ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: const BottomNavigation(),
      
      routes: {
        '/installation_sportive_detail': (context) => InstallationSportiveDetail(),
      },
    );
  }
}
