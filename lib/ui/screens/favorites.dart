import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_projet_final/bloc/favorites_list_cubit.dart';
import '../../bloc/location_cubit.dart';
import '../../models/installation_sportive.dart';
import 'installation_sportive_card.dart';

class Favorites extends StatefulWidget {
  const Favorites({Key? key}) : super(key: key);
  @override
  State<Favorites> createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  List<List<InstallationSportive>> _installationSportives = [];
  LocationCubit? _locationCubit;
  late FavoritesListCubit _favoritesListCubit;

  @override
  void didChangeDependencies() {
    _favoritesListCubit = BlocProvider.of<FavoritesListCubit>(context, listen: false);
    _locationCubit = BlocProvider.of<LocationCubit>(context, listen: false);
    _favoritesListCubit.initData();
    super.didChangeDependencies();
  }
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("test");
    return Scaffold(
      appBar: AppBar(
        title: Text("Mes favoris", style: TextStyle(fontSize: 18)),
        elevation: 0,
      ),
      body: SafeArea(
       child: BlocBuilder<FavoritesListCubit, List<List<InstallationSportive>>>(
          builder: (context, installationSportives) {
            _favoritesListCubit.reorderListWithDistance(_locationCubit?.state);
            return Container(
              child: ListView.builder(
                itemCount: installationSportives.length,
                itemBuilder: (BuildContext context, int index) {
                  List<InstallationSportive> listOfInstallationSportives =
                      installationSportives[index];
                      if(installationSportives[index][0].activite != null && installationSportives[index][0].activite!.isNotEmpty){
                        return InstallationSportiveCard(listOfInstallationSportives, index);
                      }
                      return SizedBox.shrink();
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
