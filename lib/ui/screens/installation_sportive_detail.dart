import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_projet_final/models/installation_sportive.dart';
import 'package:flutter_projet_final/repository/direction_repository.dart';
import 'package:flutter_projet_final/repository/preferences_repository.dart';
import 'package:flutter_projet_final/ui/screens/installation_info.dart';
import 'package:flutter_projet_final/ui/screens/installation_row_info.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../bloc/favorites_list_cubit.dart';
import '../../bloc/location_cubit.dart';
import '../../models/directions.dart';
import '../../services/installation_sportive_service.dart';

class InstallationSportiveDetail extends StatefulWidget {
  InstallationSportiveDetail({Key? key}) : super(key: key);

  @override
  State<InstallationSportiveDetail> createState() =>
      _InstallationSportiveDetailState();
}

class _InstallationSportiveDetailState extends State<InstallationSportiveDetail> {
  LocationCubit? locationCubit;
  late FavoritesListCubit _favoritesListCubit;
  late GoogleMapController mapController;
  late InstallationSportive installationSportive;
  LatLng? _currentPosition;
  LatLng? _destination;
  final InstallationSportiveService _installationSportiveService = InstallationSportiveService();
  final PreferencesRepository _preferencesRepository = PreferencesRepository();
  final ValueNotifier<Directions?> _directions = ValueNotifier(null);
  final ValueNotifier<bool> _isInFavorite = ValueNotifier(false);
  // bool _isInFavorite = false;

  @override
  void didChangeDependencies() {
    locationCubit = BlocProvider.of<LocationCubit>(context);
    _favoritesListCubit = BlocProvider.of<FavoritesListCubit>(context);
        installationSportive =
        ModalRoute.of(context)?.settings.arguments as InstallationSportive;
        if (locationCubit!.state != null) {
      _currentPosition = LatLng(
          locationCubit!.state!.latitude!, locationCubit!.state!.longitude!);
      _destination = LatLng(installationSportive.geoPoint2d!.latitude,
          installationSportive.geoPoint2d!.longitude);
      DirectionsRepository()
          .getDirections(
              currentPosition: _currentPosition!, destination: _destination!)
          .then((directions) {
        _directions.value = directions;
      });
      _isInFavorite.value = _favoritesListCubit.checkIfInstallationIsInFavorites(installationSportive);
    }

    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            _installationSportiveService
                .formatStringInUpperCamelCase(installationSportive.nomInstal!),
            style: TextStyle(fontSize: 17),
          ),
          actions: [
            PopupMenuButton(
                itemBuilder: (context) => [
                      PopupMenuItem(
                        child: ValueListenableBuilder<bool>(
                          valueListenable: _isInFavorite,
                          builder: (context, isFav, child) {
                            return Text(!isFav ? 'Ajouter aux favoris' : 'Retirer des favoris');
                          },
                        ),
                        // _isInFavorite ? Text("Supprimer des favoris") : Text("Ajouter au favoris"),
                        value: 1,
                        onTap: () {
                          if(_isInFavorite.value){
                            _favoritesListCubit.removeFavorite(installationSportive);
                            final snackBar = SnackBar(
                            content: const Text('Retirer des favoris'),
                            action: SnackBarAction(
                              label: 'Fermer',
                              onPressed: () {},
                            ),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          }
                          else{
                          _favoritesListCubit.addFavorite(installationSportive);
                          final snackBar = SnackBar(
                            content: const Text('Ajouté au favoris'),
                            action: SnackBarAction(
                              label: 'Fermer',
                              onPressed: () {},
                            ),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          }
                          _isInFavorite.value = !_isInFavorite.value;
                        },
                      )
                    ])
          ],
        ),
        body: Container(
          margin: EdgeInsets.only(top: 20),
          decoration: BoxDecoration(),
          child: Column(
            children: [
              Text(
                "${_installationSportiveService.formatStringInUpperCamelCase(installationSportive.nomEquip!)}",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.lightBlue),
              ),
              ValueListenableBuilder<Directions?>(
                  valueListenable: _directions,
                  builder: (context, directions, w) {
                    if (directions != null)
                      return InstallationDetailRow(installationDetailInfos: [
                        InstallationInfo(
                          infoTitle: "Distance",
                          infoContent: directions.totalDistance,
                          widthDivider: 2,
                          leadingIcon: Icons.roundabout_right,
                        ),
                        InstallationInfo(
                            infoTitle: "Temps de trajet",
                            infoContent: directions.totalDuration,
                            widthDivider: 2,
                            leadingIcon: Icons.access_time),
                      ]);
                    else
                      return SizedBox.shrink();
                  }),
              ValueListenableBuilder<Directions?>(
                  valueListenable: _directions,
                  builder: (context, directions, w) {
                    if (directions != null)
                      return SizedBox(
                        height: MediaQuery.of(context).size.height / 3,
                        width: MediaQuery.of(context).size.width,
                        child: GoogleMap(
                            onMapCreated: _onMapCreated,
                            initialCameraPosition: CameraPosition(
                              target: LatLng(locationCubit!.state!.latitude!,
                                  locationCubit!.state!.longitude!),
                              zoom: 12.0,
                            ),
                            markers: {
                              Marker(
                                markerId: MarkerId("currentPosition"),
                                position: _currentPosition!,
                                infoWindow: InfoWindow(
                                  title: "Ma position",
                                ),
                              ),
                              Marker(
                                markerId: MarkerId("destination"),
                                position: _destination!,
                                infoWindow: InfoWindow(
                                  title: "${installationSportive.nomInstal}",
                                ),
                                icon: BitmapDescriptor.defaultMarkerWithHue(
                                    BitmapDescriptor.hueAzure),
                              ),
                            },
                            polylines: {
                              if (directions != null)
                                Polyline(
                                  polylineId: PolylineId('overview_polyline'),
                                  points: directions.polylinePoints
                                      .map((step) =>
                                          LatLng(step.latitude, step.longitude))
                                      .toList(),
                                  color: Colors.red,
                                  width: 5,
                                )
                            }),
                      );
                    else
                      return SizedBox.shrink();
                  }),
              InstallationDetailRow(
                installationDetailInfos: [
                  InstallationInfo(
                      infoTitle: "Mise en Service",
                      infoContent: installationSportive.dateService!),
                  InstallationInfo(
                      infoTitle: "Nb. place",
                      infoContent: installationSportive.nbPlace!),
                  InstallationInfo(
                      infoTitle: "Nb. vestiaire",
                      infoContent: installationSportive.nbVestiaire!)
                ],
              ),
              InstallationDetailRow(
                installationDetailInfos: [
                  InstallationInfo(
                      infoTitle: "Nature",
                      infoContent: _installationSportiveService
                          .formatStringInUpperCamelCase(
                              installationSportive.natLibe!),
                      widthDivider: 2),
                  InstallationInfo(
                      infoTitle: "Nature du sol",
                      infoContent: _installationSportiveService
                          .formatStringInUpperCamelCase(
                              installationSportive.natSol!),
                      widthDivider: 2),
                ],
              ),
              InstallationDetailRow(
                installationDetailInfos: [
                  InstallationInfo(
                      infoTitle: "Type installation",
                      infoContent: _installationSportiveService
                          .formatStringInUpperCamelCase(
                              installationSportive.nomFamEq!),
                      widthDivider: 1),
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }
}
