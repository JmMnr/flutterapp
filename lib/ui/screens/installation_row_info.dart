import 'package:flutter/material.dart';
import 'installation_info.dart';

class InstallationDetailRow extends StatelessWidget {
  late List<InstallationInfo> _installationInfos;
  InstallationDetailRow({required List<InstallationInfo> installationDetailInfos, Key? key}) : super(key: key) {
    _installationInfos = installationDetailInfos;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: _installationInfos)
    );
  }
}
