import 'package:flutter/material.dart';

class InstallationInfo extends StatelessWidget {
  late String _infoTitle;
  late String _infoContent;
  late int _widthDivider;
  IconData? _leadingIcon;
  InstallationInfo({required String infoTitle, required String infoContent, int widthDivider = 3, IconData? leadingIcon = null, Key? key}) : super(key: key){
    _infoTitle = infoTitle;
    _infoContent = infoContent;
    _widthDivider = widthDivider;
    _leadingIcon = leadingIcon;
  }
  @override
  Widget build(BuildContext context) {
    
    return Column(
      children: [
        SizedBox(
          width: (MediaQuery.of(context).size.width - 20) / _widthDivider,
          child: Expanded(
            child: ListTile(
              leading: _leadingIcon != null ? Container(height: double.infinity, width : 20, child: Icon(_leadingIcon!, color: Colors.green)) : null,
              title: Text(_infoContent, style: TextStyle(color: Colors.lightBlue),),
              subtitle: Text(_infoTitle),
              minLeadingWidth: 10,
            ),
          ),
        ),
      ]
    );
  }
}