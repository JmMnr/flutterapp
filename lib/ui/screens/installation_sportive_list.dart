import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_projet_final/models/installation_sportive.dart';
import 'package:flutter_projet_final/ui/screens/installation_sportive_card.dart';

import '../../bloc/installation_sportive_list_cubit.dart';
import '../../bloc/location_cubit.dart';

//https://data.angers.fr/api/records/1.0/search/?dataset=equipements-sportifs-angers&q=&facet=categorie&facet=nom_fam_eq&facet=equip_2&facet=angers_stadium
//https://data.angers.fr/explore/dataset/equipements-sportifs-angers/information/?location=17,47.47032,-0.52178&basemap=jawg.streets

class InstallationSportiveList extends StatefulWidget {
  const InstallationSportiveList({Key? key}) : super(key: key);

  @override
  State<InstallationSportiveList> createState() => _InstallationSportiveList();
}

class _InstallationSportiveList extends State<InstallationSportiveList> {
  InstallationSportiveCubit? installationSportiveCubit;
  LocationCubit? locationCubit;
  
  @override
  Future<void> didChangeDependencies() async {
    installationSportiveCubit = BlocProvider.of<InstallationSportiveCubit>(context, listen: false);
    locationCubit = BlocProvider.of<LocationCubit>(context, listen: false);
    installationSportiveCubit!.initData();
    locationCubit!.getCurrentLocation();
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liste des installations sportives", style: TextStyle(fontSize: 18)),
        elevation: 0,
        actions: [
          // IconButton(onPressed: () => {}, icon: Icon(Icons.search)) TODO implémenter des filtres ?
          ]),
      body: SafeArea(
        child: BlocBuilder<InstallationSportiveCubit, List<List<InstallationSportive>>>(
          builder: (context, installationSportives) {
            installationSportiveCubit!.reorderListWithDistance(locationCubit?.state);
            return Container(
              child: ListView.builder(
                itemCount: installationSportives.length,
                itemBuilder: (BuildContext context, int index) {
                  List<InstallationSportive> listOfInstallationSportives =
                      installationSportives[index];
                      if(installationSportives[index][0].activite != null && installationSportives[index][0].activite!.isNotEmpty){
                        return InstallationSportiveCard(listOfInstallationSportives, index);
                      }
                      return SizedBox.shrink();
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
