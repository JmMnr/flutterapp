import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/location.dart';

import '../../bloc/location_cubit.dart';
import '../../models/installation_sportive.dart';
import '../../services/installation_sportive_service.dart';

class InstallationSportiveCard extends StatelessWidget {
  List<InstallationSportive> _installationSportiveList = [];
  final ValueNotifier<bool> _showDropdown = ValueNotifier(false);
    final InstallationSportiveService _installationSportiveService = InstallationSportiveService();

  int _index = 0;
  InstallationSportiveCard(List<InstallationSportive> listOfInstallationSportives, int index) {
    _installationSportiveList = listOfInstallationSportives;
    _index = _index;
  }

  Icon getCardIcon(natureEquipement){
    switch (natureEquipement){
      case "INTERIEUR" : case "EXTERIEUR COUVERT":
        return Icon(Icons.directions_run, color: Colors.blue);
      case "DECOUVERT" : case "SITE NATUREL AMENAGE": case "SITE NATUREL":
        return Icon(Icons.directions_run, color: Colors.green);
      default:
        return Icon(Icons.directions_run, color: Colors.red);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 15,
        margin: EdgeInsets.symmetric(vertical: 7.5, horizontal: 20),
        shape: ShapeBorder.lerp(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            0.5),
        color: Color.fromARGB(255, 52, 52, 52),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: Color.fromARGB(255, 64, 64, 64),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10))),
              padding: EdgeInsetsGeometry.lerp(
                  EdgeInsets.only(left: 10, top: 10, bottom: 10),
                  EdgeInsets.only(left: 10, top: 10, bottom: 10),
                  0.5),
              child: Row(

                children: [
                  Container(
                      child: getCardIcon(_installationSportiveList[_index].natLibe),
                      margin: EdgeInsets.symmetric(horizontal: 5)),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Text(
                        _installationSportiveService.formatStringInUpperCamelCase(_installationSportiveList[_index].nomInstal!),
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              onTap: () {
                if (_installationSportiveList.length == 1) {
                  Navigator.pushNamed(context, '/installation_sportive_detail',
                      arguments: _installationSportiveList[_index]);
                } else {
                  _showDropdown.value = !_showDropdown.value;
                }
              },
              title: _installationSportiveList[0].activite != null
                  ? Text(
                      _installationSportiveList.length == 1 ? _installationSportiveService.formatStringInUpperCamelCase(_installationSportiveList[0].activite!) : "Séléctionner une activité",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    )
                  : null,
              subtitle: BlocBuilder<LocationCubit, LocationData?>(
                builder: (context, locationState) => (locationState != null && _installationSportiveList[0].distanceBetweenMyPosition != null)
                    ? Text(
                        "A ${_installationSportiveList[0].distanceBetweenMyPosition}m de moi")
                    : SizedBox.shrink(),
              ),
              leading: Container(height: double.infinity, child: Icon(Icons.sports_soccer)),
              trailing: _installationSportiveList.length > 1
                  ? ValueListenableBuilder<bool>(
                    valueListenable: _showDropdown,
                    builder: (context, isArrownDown, w) {
                      if(!isArrownDown)
                      return Icon(Icons.keyboard_arrow_right, size: 28);
                      else
                      return Icon(
                        Icons.keyboard_arrow_down, size: 28);
                    },
                  )
                  : Icon(Icons.keyboard_arrow_right, size: 28,
                      ),
            ),
            ValueListenableBuilder<bool>(
                valueListenable: _showDropdown,
                builder: (context, shouldShow, w) {
                  if (!shouldShow)
                    return SizedBox.shrink();
                  else
                    return ListView.separated(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: _installationSportiveList.length,
                        itemBuilder: (BuildContext con, int i) {
                          return ListTile(
                              title:
                                  Text(_installationSportiveService.formatStringInUpperCamelCase(_installationSportiveList[i].activite!)),
                              trailing: Icon(Icons.keyboard_arrow_right),
                              onTap: () {
                                Navigator.pushNamed(
                                    context, '/installation_sportive_detail',
                                    arguments: _installationSportiveList[i]);
                              });
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(
                                color: Colors.lightBlue,
                                indent: 15,
                                endIndent: 15));
                })
          ],
        ));
  }
}
