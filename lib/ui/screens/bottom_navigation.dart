import 'package:flutter/material.dart';
import 'package:flutter_projet_final/ui/screens/favorites.dart';

import 'installation_sportive_list.dart';

class BottomNavigation extends StatefulWidget {
  const BottomNavigation({Key? key}) : super(key: key);

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _selectedIndex = 0;
  Widget _installationSportiveList = InstallationSportiveList();
  Widget _favorites = Favorites();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: this.getBody(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: 'Favoris',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.lightBlue,
          onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget getBody(int index) {
    if (index == 0) {
      return _installationSportiveList;
    } else if (index == 1) {
      return _favorites;
    }
    return _installationSportiveList;
  }
}