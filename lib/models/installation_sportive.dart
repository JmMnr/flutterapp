import 'package:latlong2/latlong.dart';

class InstallationSportive {
  ///Identifiant de l’équipement sportif, de l’espace ou du site de pratiques
  final String? resFid;

  ///Code de l’objet géographique RES
  final String? codeInstal;

  ///Nom de l’objet géographique RES
  final String? nomInstal;

  ///Code de l’équipement RES
  final String? codeEquip;

  ///Nom de l’équipement RES
  final String? nomEquip;

  ///Nombre d’équipements
  final String? nombre;

  ///Catégorie de l’installation sportive
  final String? categorie;

  ///Code de la famille d’équipements sportifs
  final String? codeFamEq;

  ///Nom de la famille d’équipements sportifs
  final String? nomFamEq;

  ///Code du type d’équipement
  final String? equip1;

  ///Nom du type d’équipement
  final String? equip2;

  ///Nature du sol
  final String? natSol;

  ///Nature
  final String? natLibe;

  ///Nombre de place en tribune
  final String? nbPlace;

  ///Nombre de vestiaire
  final String? nbVestiaire;

  ///Equipement de convivialité
  final String? eqConvivial;

  ///Date de mise en service
  final String? dateService;

  ///Date d’intégration au RES
  final String? dateRef;

  ///Activités pratiquées
  final String? activite;

  ///Lien vers la fiche RES
  final String? url;

  ///Mail du Ministère des Sports
  final String? mail;

  ///Code INSEE
  final String? insee;

  ///Nom de l’exploitant
  final String? exploit;

  ///Source de la donnée
  final String? source;

  ///Nom de l’objet géographique
  final String? nom;

  ///Nom de la commune
  final String? nomCom;

  ///Coordonnées géographique
  final LatLng? geoPoint2d;

  ///Distance séparant le lieu dit et ma position gps
  int? distanceBetweenMyPosition;

  InstallationSportive(
      this.resFid,
      this.codeInstal,
      this.nomInstal,
      this.codeEquip,
      this.nomEquip,
      this.nombre,
      this.categorie,
      this.codeFamEq,
      this.nomFamEq,
      this.equip1,
      this.equip2,
      this.natSol,
      this.natLibe,
      this.nbPlace,
      this.nbVestiaire,
      this.eqConvivial,
      this.dateService,
      this.dateRef,
      this.activite,
      this.url,
      this.mail,
      this.insee,
      this.exploit,
      this.source,
      this.nom,
      this.nomCom,
      this.geoPoint2d);

  factory InstallationSportive.fromJson(Map<String, dynamic> json) {
    final Map<String, dynamic> properties = json['fields'] ?? {};
    final String? resFid = properties['res_fid'];
    final String? codeInstal = properties['code_instal'];
    final String? nomInstal = properties['nom_instal'];
    final String? codeEquip = properties['code_equip'];
    final String? nomEquip = properties['nom_equip'];
    final String? nombre = properties['nombre'];
    final String? categorie = properties['categorie'];
    final String? codeFamEq = properties['code_fam_eq'];
    final String? nomFamEq = properties['nom_fam_eq'];
    final String? equip1 = properties['equip_1'];
    final String? equip2 = properties['equip_2'];
    final String? natSol = properties['nat_sol'];
    final String? natLibe = properties['nat_libe'];
    final String? nbPlace = properties['nb_place'];
    final String? nbVestiaire = properties['nb_vestiaire'];
    final String? eqConvivial = properties['eq_convivial'];
    final String? dateService = properties['date_service'];
    final String? dateRef = properties['date_ref'];
    final String? activite = properties['activite'];
    final String? url = properties['url'];
    final String? mail = properties['mail'];
    final String? insee = properties['insee'];
    final String? exploit = properties['exploit'];
    final String? source = properties['source'];
    final String? nom = properties['nom'];
    final String? nomCom = properties['nom_com'];
    final LatLng? geoPoint2d =
        LatLng(properties['geo_point_2d'][0], properties['geo_point_2d'][1]);

    return InstallationSportive(
        resFid,
        codeInstal,
        nomInstal,
        codeEquip,
        nomEquip,
        nombre,
        categorie,
        codeFamEq,
        nomFamEq,
        equip1,
        equip2,
        natSol,
        natLibe,
        nbPlace,
        nbVestiaire,
        eqConvivial,
        dateService,
        dateRef,
        activite,
        url,
        mail,
        insee,
        exploit,
        source,
        nom,
        nomCom,
        geoPoint2d);
  }

  Map<String, dynamic> toJson() {
    return {
      'resFid': resFid,
      'codeInstal': codeInstal,
      'nomInstal': nomInstal,
      'codeEquip': codeEquip,
      'nomEquip': nomEquip,
      'nombre': nombre,
      'categorie': categorie,
      'codeFamEq': codeFamEq,
      'nomFamEq': nomFamEq,
      'equip1': equip1,
      'equip2': equip2,
      'natSol': natSol,
      'natLibe': natLibe,
      'nbPlace': nbPlace,
      'nbVestiaire': nbVestiaire,
      'eqConvivial': eqConvivial,
      'dateService': dateService,
      'dateRef': dateRef,
      'activite': activite,
      'url': url,
      'mail': mail,
      'insee': insee,
      'exploit': exploit,
      'source': source,
      'nom': nom,
      'nomCom': nomCom,
      'latitude': geoPoint2d!.latitude,
      'longitude': geoPoint2d!.longitude,
      'distanceBetweenMyPosition': distanceBetweenMyPosition
    };
  }

  factory InstallationSportive.fromPreferencesJson(Map<String, dynamic> json) {
    return InstallationSportive(
        json['resFid'],
        json['codeInstal'],
        json['nomInstal'],
        json['codeEquip'],
        json['nomEquip'],
        json['nombre'],
        json['categorie'],
        json['codeFamEq'],
        json['nomFamEq'],
        json['equip1'],
        json['equip2'],
        json['natSol'],
        json['natLibe'],
        json['nbPlace'],
        json['nbVestiaire'],
        json['eqConvivial'],
        json['dateService'],
        json['dateRef'],
        json['activite'],
        json['url'],
        json['mail'],
        json['insee'],
        json['exploit'],
        json['source'],
        json['nom'],
        json['nomCom'],
        LatLng(json['latitude'], json['longitude']));
  }
}
