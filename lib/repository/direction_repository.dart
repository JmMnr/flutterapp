import 'package:dio/dio.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../models/directions.dart';

class DirectionsRepository {
  final Dio _dio = Dio();
  Future<Directions?> getDirections(
      {required LatLng currentPosition, required LatLng destination}) async {
    final response = await _dio.get(
        'https://maps.googleapis.com/maps/api/directions/json?',
        queryParameters: {
          'origin': '${currentPosition.latitude},${currentPosition.longitude}',
          'destination': '${destination.latitude},${destination.longitude}',
          'key': 'AIzaSyAvMGbVS1R7Mjzx8kcmse5nquq7bCSiUiA'
        });
    if (response.statusCode == 200) {
      return Directions.fromJson(response.data);
    }
    return null;
  }
}
