import 'dart:convert';

import 'package:flutter_projet_final/models/installation_sportive.dart';
import 'package:http/http.dart';

class InstallationSportiveRepository{
  Future<List<InstallationSportive>> fetchInstallationSportives() async {
final Response response =
        await get(Uri.parse('https://data.angers.fr/api/records/1.0/search/?dataset=equipements-sportifs-angers&q=&rows=2500&facet=categorie&facet=nom_fam_eq&facet=equip_2&facet=angers_stadium'));
    if (response.statusCode == 200) {
      final List<InstallationSportive> installationSportiveList = [];
      final Map<String, dynamic> json = jsonDecode(response.body);
      if (json.containsKey("records")) {
        final List<dynamic> records = json['records'];
        records.forEach((record) {
          installationSportiveList.add(InstallationSportive.fromJson(record));
        });
      }
      return installationSportiveList;
    } else {
      throw Exception('Failed to load addresses');
    }
  }
}
