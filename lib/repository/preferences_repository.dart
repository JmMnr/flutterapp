import 'dart:convert';

import 'package:flutter_projet_final/models/installation_sportive.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {
  Future<void> saveInstallationSportives(List<InstallationSportive> installationSportives) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> listJson = [];
    for (final InstallationSportive installationSportive in installationSportives) {
      listJson.add(jsonEncode(installationSportive.toJson()));
    }
    prefs.setStringList('installationSportives', listJson);
  }

  Future<List<InstallationSportive>> loadInstallationSportives() async {
        final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<InstallationSportive> installationSportives = [];
    List<dynamic>? json = prefs.getStringList('installationSportives');
    json!.forEach((element) {
      installationSportives.add(InstallationSportive.fromPreferencesJson(jsonDecode(element)));
    });
    return installationSportives;
  }
}


// Future<void> saveCompanies(List<Company> companies) async {
//     final SharedPreferences prefs = await SharedPreferences.getInstance();
//     final List<String> listJson = [];
//     for (final Company company in companies) {
//       listJson.add(jsonEncode(company.toJson()));
//     }
//     prefs.setStringList('companies', listJson);
//   }

//   Future<List<Company>> loadCompanies() async {
//     final SharedPreferences prefs = await SharedPreferences.getInstance();
//     List<Company> companies = [];
//     List<dynamic>? json = prefs.getStringList('companies');
//     json!.forEach((element) {
//       companies.add(Company.fromJson(jsonDecode(element)));
//     });
//     // A compléter
//     return companies;
//   }
