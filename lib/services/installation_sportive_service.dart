class InstallationSportiveService{
  String formatStringInUpperCamelCase(String string){
    List<String> words = string.split(' ');
    String result = '';
    words.map((e) => result += e[0].toUpperCase() + e.substring(1).toLowerCase() + ' ').toList();
    return result;

  }
}