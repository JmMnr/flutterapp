import 'package:bloc/bloc.dart';
import 'package:location/location.dart';

class LocationCubit extends Cubit<LocationData?> {
  LocationCubit() : super(null);
  Location _location = Location();
  late bool _serviceEnabled;
  late PermissionStatus _permissionGranted;

  void getCurrentLocation() async {
    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    emit(await _location.getLocation());
  }
}
