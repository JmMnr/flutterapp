import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import '../models/installation_sportive.dart';
import '../repository/installation_sportive_repository.dart';

class InstallationSportiveCubit extends Cubit<List<List<InstallationSportive>>>{
    InstallationSportiveCubit(this._installationRepository) : super([]);
    final InstallationSportiveRepository _installationRepository;

    Future<void> initData() async {
      List<List<InstallationSportive>> listOfInstallations = [];
      groupBy(await _installationRepository.fetchInstallationSportives(), (InstallationSportive obj) => obj.nomInstal).forEach((key, value) {
        listOfInstallations.add(value);
      });
      emit(listOfInstallations);
    }

    void reorderListWithDistance(LocationData? currentLocation){
      if(currentLocation == null){
        return;
      }
      state.forEach((installationSportive) {
        installationSportive[0].distanceBetweenMyPosition = Geolocator.distanceBetween(installationSportive[0].geoPoint2d!.latitude, installationSportive[0].geoPoint2d!.longitude, currentLocation.latitude!, currentLocation.longitude!).toInt();
      });
      state.sort(((a, b) => a[0].distanceBetweenMyPosition!.compareTo(b[0].distanceBetweenMyPosition!)));
      emit(state);
    }


}