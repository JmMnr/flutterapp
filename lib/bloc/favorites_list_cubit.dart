import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:flutter_projet_final/models/installation_sportive.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';

import '../repository/preferences_repository.dart';

class FavoritesListCubit extends Cubit<List<List<InstallationSportive>>> {
  FavoritesListCubit() : super([]);
  final PreferencesRepository _preferencesRepository = PreferencesRepository();

  initData() async {
    List<List<InstallationSportive>> installationSportives = [];
    groupBy(await _preferencesRepository.loadInstallationSportives(),
        (InstallationSportive obj) => obj.nomInstal).forEach((key, value) {
      installationSportives.add(value);
    });
    emit(installationSportives);
  }

  void addFavorite(InstallationSportive installationSportive) async {
    List<InstallationSportive> installationSportives =
        await _preferencesRepository.loadInstallationSportives();
    installationSportives.add(installationSportive);
    _preferencesRepository.saveInstallationSportives(installationSportives);
    emit(state..add([installationSportive]));
  }

  void removeFavorite(InstallationSportive installationSportive) async {
    List<InstallationSportive> installationSportives =
        await _preferencesRepository.loadInstallationSportives();
    installationSportives.removeWhere((x) => x.resFid == installationSportive.resFid && x.activite == installationSportive.activite);
    _preferencesRepository.saveInstallationSportives(installationSportives);
    emit(state..remove([installationSportive]));
  }

  void reorderListWithDistance(LocationData? currentLocation) {
    if (currentLocation == null) {
      return;
    }
    state.forEach((installationSportive) {
      installationSportive[0].distanceBetweenMyPosition =
          Geolocator.distanceBetween(
                  installationSportive[0].geoPoint2d!.latitude,
                  installationSportive[0].geoPoint2d!.longitude,
                  currentLocation.latitude!,
                  currentLocation.longitude!)
              .toInt();
    });
    state.sort(((a, b) => a[0]
        .distanceBetweenMyPosition!
        .compareTo(b[0].distanceBetweenMyPosition!)));
    emit(state);
  }

  bool checkIfInstallationIsInFavorites(
      InstallationSportive installationSportive) {
    return state.any(
        (listOfInstallationSportives) => listOfInstallationSportives.any((installation) => installation.resFid == installationSportive.resFid && installation.activite == installationSportive.activite));
  }
}
